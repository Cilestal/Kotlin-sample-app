// app Version
val (versionCodeMajor, versionCodeRelease, versionCodePatch) = "$version".split(".")

val versionCode: Int by extra {
    versionCodeMajor.toInt() * 10_000 + versionCodeRelease.toInt() * 100 + versionCodePatch.toInt()
}

val versionName by extra { "${version}" }