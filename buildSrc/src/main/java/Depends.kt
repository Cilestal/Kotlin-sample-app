object Depends {
    object Kotlin {
        val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    }

    object AndroidSupport {
        val annotation = "com.android.support:support-annotations:${Versions.supportLibrary}"
        val appcompat_v7 = "com.android.support:appcompat-v7:${Versions.supportLibrary}"
        val design = "com.android.support:design:${Versions.supportLibrary}"
        val recyclerView = "com.android.support:recyclerview-v7:${Versions.supportLibrary}"
        val cardview_v7 = "com.android.support:cardview-v7:${Versions.supportLibrary}"

        val support_v4 = "com.android.support:support-v4:${Versions.supportLibrary}"
        val support_v13 = "com.android.support:support-v13:${Versions.supportLibrary}"

        val customtabs = "com.android.support:customtabs:${Versions.supportLibrary}"
        val multidex = "com.android.support:multidex:1.0.2"
        val support_emoji = "com.android.support:support-emoji-appcompat:${Versions.supportLibrary}"
        val preference_v7 = "com.android.support:preference-v7:${Versions.supportLibrary}"
        val preference_v14 = "com.android.support:preference-v14:${Versions.supportLibrary}"

        val constraintLayout = "com.android.support.constraint:constraint-layout:1.1.2"
    }

    object Injection {
        val kodein = "org.kodein.di:kodein-di-generic-jvm:${Versions.kodein}"
        val kodeinAndroid = "org.kodein.di:kodein-di-framework-android:${Versions.kodein}"
    }

    object Network {
        val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
        val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
        val retrofitRxJava2 = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"

        val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
        val okhttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"

        val fuel = "com.github.kittinunf.fuel:fuel:${Versions.fuel_version}" //for JVM
        val fuelAndroid = "com.github.kittinunf.fuel:fuel-android:${Versions.fuel_version}" //for Android
        val fuelRxJava = "com.github.kittinunf.fuel:fuel-rxjava:${Versions.fuel_version}" //for RxJava support
        val fuelGson = "com.github.kittinunf.fuel:fuel-gson:${Versions.fuel_version}" //for Gson support
    }

    object Serialization {
        val gson = "com.google.code.gson:gson:2.8.5"
    }

    object Async {
        val rxAndroid = "io.reactivex.rxjava2:rxandroid:2.0.2"
        val rxKotlin = "io.reactivex.rxjava2:rxkotlin:2.2.0"
        val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
        val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    }

    object Logging {
        val timber = "com.jakewharton.timber:timber:4.7.1"
    }

    object Media {
        val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
        val glideAnnotation = "com.github.bumptech.glide:compiler:${Versions.glide}"
    }

    object Test {
        val junit = "junit:junit:4.12"
        val androidTestRunner = "com.android.support.test:runner:1.0.2"
        val espressoCore = "com.android.support.test.espresso:espresso-core:3.0.2"
    }
}

