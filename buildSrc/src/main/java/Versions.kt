import org.gradle.api.JavaVersion

object Versions {
    // app info
    const val defaultMinSdkVersion = 19
    const val defaultCompileSdkVersion = 27
    const val defaultTargetSdkVersion = 27
    const val defaultApplicationId = "ua.dp.michaellang.kotlinapp1"

    // languages
    const val kotlin = "1.2.51"
    val defaultJava = JavaVersion.VERSION_1_8

    // deps
    const val supportLibrary = "27.1.1"
    const val kodein = "5.1.0"
    const val retrofit = "2.4.0"
    const val okhttp = "3.10.0"
    const val coroutines = "0.23.4"
    const val glide = "4.7.1"
    const val fuel_version = "1.14.0"
}
