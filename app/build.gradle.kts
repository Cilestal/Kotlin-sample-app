import com.android.build.gradle.internal.api.BaseVariantOutputImpl
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.config.KotlinCompilerVersion

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Versions.defaultCompileSdkVersion)
    defaultConfig {
        applicationId = Versions.defaultApplicationId
        minSdkVersion(Versions.defaultMinSdkVersion)
        targetSdkVersion(Versions.defaultTargetSdkVersion)
        versionCode = extra["versionCode"] as Int
        versionName = extra["versionName"] as String
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    compileOptions {
        setSourceCompatibility(Versions.defaultJava)
        setTargetCompatibility(Versions.defaultJava)
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

//    applicationVariants.all { variant ->
//        val outputFileAppName = "app"
//        variant.outputs.all { output ->
//            val outputImpl = output as BaseVariantOutputImpl
//            outputImpl.outputFileName = "vaka.apk"
//            //outputFileName = ""
//            true
//        }
//    }

    packagingOptions {
        exclude("META-INF/main.kotlin_module")
    }
}

kotlin { // configure<org.jetbrains.kotlin.gradle.dsl.KotlinProjectExtension>
    experimental.coroutines = Coroutines.ENABLE
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Depends.Kotlin.kotlinStdlib)
    implementation(Depends.AndroidSupport.appcompat_v7)
    implementation(Depends.AndroidSupport.constraintLayout)
    implementation(Depends.AndroidSupport.recyclerView)

    implementation(Depends.Network.retrofit)
    implementation(Depends.Network.retrofitGsonConverter)
    implementation(Depends.Network.retrofitRxJava2)
    implementation(Depends.Network.okhttp)
    implementation(Depends.Network.okhttpLoggingInterceptor)

    implementation(Depends.Async.rxAndroid)
    implementation(Depends.Async.rxKotlin)

    implementation(Depends.Network.fuel)
    implementation(Depends.Network.fuelAndroid)
    implementation(Depends.Network.fuelGson)
    implementation(Depends.Network.fuelRxJava)

    implementation(Depends.Async.coroutines)
    implementation(Depends.Async.coroutinesAndroid)

    implementation(Depends.Injection.kodein)
    implementation(Depends.Injection.kodeinAndroid)

    implementation(Depends.Media.glide)
    kapt(Depends.Media.glideAnnotation)

    implementation(Depends.Logging.timber)
    implementation(Depends.Serialization.gson)

    testImplementation(Depends.Test.junit)
    androidTestImplementation(Depends.Test.androidTestRunner)
    androidTestImplementation(Depends.Test.espressoCore)
}