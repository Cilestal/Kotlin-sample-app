package ua.dp.michaellang.kotlinapp1

import android.app.Application
import org.kodein.di.KodeinAware
import timber.log.Timber
import ua.dp.michaellang.kotlinapp1.di.AppModule

class App : Application(), KodeinAware {

    companion object {
        const val TAG_APP_CONTEXT = "appContext"
        const val TAG_CACHE_DIR = "cacheDir"
    }

    override val kodein = AppModule.module(this@App)

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        configureFuel()
    }

    private fun configureFuel() {

    }
}