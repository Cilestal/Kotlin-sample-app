package vrgsoft.net.wellnessadmin.utils

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.rx.rx_object
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import timber.log.Timber
import ua.dp.michaellang.kotlinapp1.di.GlideApp

/**fragment manager*/

fun FragmentManager.replaceWithBackStack(containerId: Int, fragment: Fragment) {
    beginTransaction()
            .replace(containerId, fragment, fragment::class.java.simpleName)
            .addToBackStack(fragment::class.java.simpleName)
            .commit()
}

fun View.show(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun ImageView.setImageUrl(url: String?) {
    GlideApp.with(context)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}

/**fragments*/
fun Fragment.showKeyboard(view: View) {
    val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

    inputManager?.showSoftInput(view, 0)
}

fun Fragment.hideKeyboard() {
    val inputManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

    if (view != null) inputManager?.hideSoftInputFromWindow(view?.windowToken, 0)
}

/* FUEL */

inline fun <reified T : Any> Single<Result<T, FuelError>>.applyFuelErrorHandler(): Single<T> {
    return this.flatMap { t: Result<T, FuelError> ->
        when (t) {
            is Result.Failure -> {
                Timber.tag("HttpError").e(t.getException().response.toString())
                Single.error(t.getException())
            }
            is Result.Success -> Single.just(t.get())
        }
    }
}

inline fun <reified T : Any> Request.toSingle() = rx_object(jsonDeserializable<T>()).applyFuelErrorHandler()

inline fun <reified T : Any> jsonDeserializable() = object : ResponseDeserializable<T> {
    override fun deserialize(content: String): T {
        return Gson().fromJson<T>(content, object : TypeToken<T>() {}.type)
    }
}
