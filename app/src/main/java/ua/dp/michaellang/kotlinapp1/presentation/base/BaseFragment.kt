package ua.dp.michaellang.kotlinapp1.presentation.base

import android.support.v4.app.Fragment
import org.kodein.di.Copy
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class BaseFragment : Fragment(), KodeinAware {
    private val parentKodein by closestKodein()

    override val kodein: Kodein = Kodein.lazy {
        extend(parentKodein, copy = Copy.All)
        import(module)
    }

    abstract val module : Kodein.Module
}