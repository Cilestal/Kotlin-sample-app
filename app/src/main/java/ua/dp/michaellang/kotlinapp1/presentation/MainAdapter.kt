package ua.dp.michaellang.kotlinapp1.presentation

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class MainAdapter(override val kodein: Kodein) : RecyclerView.Adapter<MainAdapter.MainHolder>(), KodeinAware {

    val inflater: LayoutInflater by instance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder = MainHolder(null)

    override fun getItemCount(): Int = 0

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        println("hello")
    }

    inner class MainHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    }
}