package ua.dp.michaellang.kotlinapp1.di

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
open class GlideKotlinApp : AppGlideModule()