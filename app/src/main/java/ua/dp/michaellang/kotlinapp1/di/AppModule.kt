package ua.dp.michaellang.kotlinapp1.di

import android.content.Context
import org.kodein.di.Kodein
import org.kodein.di.android.androidModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import ua.dp.michaellang.kotlinapp1.App
import java.io.File

object AppModule {

    fun module(app: App) = Kodein.lazy {
        bind<Context>(App.TAG_APP_CONTEXT) with singleton { app.applicationContext }

        bind<File>(App.TAG_CACHE_DIR) with singleton { app.applicationContext.cacheDir }

        import(NetworkModule.module())

        import(androidModule(app))
    }
}