package ua.dp.michaellang.kotlinapp1.presentation

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import retrofit2.Retrofit
import ua.dp.michaellang.kotlinapp1.R
import ua.dp.michaellang.kotlinapp1.presentation.base.BaseActivity

class MainActivity : BaseActivity() {
    override val module: Kodein.Module = MainActivityModule.module()

    val retrofit: Retrofit by instance()
    val adapter: MainAdapter by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvTitle.text = "Hello world"

        supportFragmentManager.beginTransaction()
                .add(android.R.id.content, TestFragment())
                .commit()
    }
}
