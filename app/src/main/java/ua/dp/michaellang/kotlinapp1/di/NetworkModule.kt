package ua.dp.michaellang.kotlinapp1.di

import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import ua.dp.michaellang.kotlinapp1.App
import ua.dp.michaellang.kotlinapp1.Const
import java.io.File

object NetworkModule {
    fun module() = Kodein.Module("NetworkModule") {

        // OkHttp + Retrofit

        bind<Cache>() with singleton {
            val cacheDir = instance<File>(App.TAG_CACHE_DIR)
            Cache(cacheDir, 100 * 1024 * 1024)
        }

        bind<Converter.Factory>() with singleton { GsonConverterFactory.create() }

        bind<CallAdapter.Factory>() with singleton { RxJava2CallAdapterFactory.create() }

        bind<HttpLoggingInterceptor>() with singleton {
            HttpLoggingInterceptor {
                Timber.tag("OkHttp").d(it)
            }.setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        bind<OkHttpClient>() with singleton {
            OkHttpClient.Builder()
                    .addInterceptor(instance<HttpLoggingInterceptor>())
                    .cache(instance())
                    .build()
        }

        bind<Retrofit>() with singleton {
            Retrofit.Builder()
                    .client(instance())
                    .addConverterFactory(instance())
                    .addCallAdapterFactory(instance())
                    .baseUrl(Const.BASE_URL)
                    .build()
        }

    }
}