package ua.dp.michaellang.kotlinapp1.presentation

import android.os.Bundle
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import retrofit2.Retrofit
import ua.dp.michaellang.kotlinapp1.presentation.base.BaseFragment

class TestFragment : BaseFragment() {
    override val module: Kodein.Module = Kodein.Module("") {

    }

    val retrofit: Retrofit by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        println("")
    }
}