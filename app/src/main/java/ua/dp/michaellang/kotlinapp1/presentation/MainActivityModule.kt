package ua.dp.michaellang.kotlinapp1.presentation

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

object MainActivityModule {
    fun module() = Kodein.Module("MainActivityModule") {

        bind<MainAdapter>() with singleton { MainAdapter(kodein) }
    }
}