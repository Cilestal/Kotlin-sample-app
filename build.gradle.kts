// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:3.1.3")
        classpath(kotlin("gradle-plugin", version = Versions.kotlin))
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }

    apply {
        from("${rootProject.projectDir}/buildSrc/appVersion.gradle.kts")
    }
}

task<Delete>("clean") {
    delete(rootProject.buildDir)
}